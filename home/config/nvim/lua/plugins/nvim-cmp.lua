return {
    "hrsh7th/nvim-cmp",
    dependencies = {
        "hrsh7th/cmp-buffer",
        "hrsh7th/cmp-path",
        "hrsh7th/cmp-nvim-lsp",
    },
    config = function()
        local cmp = require("cmp")

        cmp.setup({
            mapping = {
                ["<C-n>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
                ["<C-p>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),

                ["<C-Space>"] = cmp.mapping.complete(),
                ["<C-e>"] = cmp.mapping.abort(),
                ["<C-a>"] = cmp.mapping.confirm({ select = true })
            },

            sources = {
                {
                    name = "nvim_lsp",
                    entry_filter = function(entry)
                        -- Remove snippet suggestions
                        return cmp.lsp.CompletionItemKind.Snippet ~= entry:get_kind()
                    end
                },
                { name = "path" },
                { name = "buffer", keyword_length = 5 }
            }
        })
    end
}

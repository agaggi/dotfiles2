return {
    "nvim-telescope/telescope.nvim",
    branch = "0.1.x",
    dependencies = { "nvim-lua/plenary.nvim" },
    config = function()
        vim.keymap.set("n", "<leader>b", ":Telescope buffers<CR>")
        vim.keymap.set("n", "<leader>f", ":Telescope find_files<CR>")
        vim.keymap.set("n", "<leader>g", ":Telescope live_grep<CR>")
        vim.keymap.set("n", "<leader>ht", ":Telescope help_tags<CR>")

        -- LSP
        vim.keymap.set("n", "gr", ":Telescope lsp_references<CR>")
        vim.keymap.set("n", "<leader>df", ":Telescope diagnostics <CR>")
    end
}

return {
    "stevearc/oil.nvim",
    config = function()
        require("oil").setup({
            view_options = {
                show_hidden = true
            }
        })

        vim.keymap.set("n", "<leader>e", ":Oil<CR>")
        vim.keymap.set("n", "<leader>s", ":new +Oil<CR>")
        vim.keymap.set("n", "<leader>v", ":vnew +Oil<CR>")
    end
}

#! /usr/bin/env python3

import os
import shutil
import subprocess

def get_distro() -> str:
    return f"{os.uname().nodename} {os.uname().machine}"


def get_uptime() -> str:
    with open("/proc/uptime", "r") as file:
        uptime = float(file.read().split()[0])

    days = int((uptime / 3600) / 24)
    hours = int((uptime / 3600) % 24)
    minutes = int((uptime / 60) % 60)

    return f"{days} days, {hours} hours, {minutes} minutes"


def get_kernel() -> str:
    return os.uname().release


def get_shell() -> str:
    return os.environ["SHELL"].split("/")[-1]


def get_package_count() -> int:
    if shutil.which("dpkg"):
        cmd = subprocess.Popen(("dpkg-query", "-f", "\".\n\"", "-W"), stdout=subprocess.PIPE)
    elif shutil.which("pacman"):
        cmd = subprocess.Popen(("pacman", "-Qq"), stdout=subprocess.PIPE)
    elif shutil.which("nixos-help"):
        cmd = subprocess.Popen(("nix-store", "-qR", "/run/current-system/sw"), stdout=subprocess.PIPE)
    elif shutil.which("rpm"):
        cmd = subprocess.Popen(("rpm", "-qa"), stdout=subprocess.PIPE)
    else:
        return 0

    return int(subprocess.check_output(("wc", "-l"), stdin=cmd.stdout).decode("utf-8"))


def get_cpu() -> str:
    with open("/proc/cpuinfo", "r") as file:
        model_name = set(filter(lambda x: "model name" in x, file.readlines()))

    return model_name.pop().split(":")[1].strip()


def get_memory() -> str:
    with open("/proc/meminfo", "r") as file:
        mem_info = dict((line.split()[0].rstrip(":"), int(line.split()[1])) for line in file.readlines())

    mem_used = int((mem_info["MemTotal"] - mem_info["MemAvailable"]) / 1024)
    return f"{mem_used}M / {int(mem_info['MemTotal'] / 1024)}M\n"


def get_term_colors() -> None:
    for style in range(2):
        for color in range(30, 38):
            print(f"\033[{style};{color}m███", end="")
        print()


def get_partitions() -> None:
    df = subprocess.check_output(("df", "-hT")).decode("utf-8")
    
    col_headers = "\n\033[1;37mPartition\tType\tSize\tUsed\tFree\tUsed %\tMountpoint\033[00m\n"
    partitions = list(filter(lambda x: x.startswith("/dev"), df.split("\n")))
    partitions = "\n".join(["\t".join(partition.split()) for partition in partitions])
    
    print("".join([col_headers, partitions]))


def main() -> None:
    titles = ["OS", "Kernel", "Shell", "Uptime", "Pkgs", "CPU", "Memory"]
    funcs = [get_distro, get_kernel, get_shell, get_uptime, get_package_count, get_cpu, get_memory]

    for title, func in zip(titles, funcs):
        print(f"\033[1;37m{title}\033[00m\t{func()}")

    get_term_colors()
    get_partitions()


if __name__ == "__main__":
    main()

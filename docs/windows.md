# Windows installation guide

Boot into your installation media for Windows and press `Shift+F10` on the starting screen. This 
will open the command prompt.

Then run `diskpart` and select the disk you want to partition (you can see all disks by using `list
disk`):

```
X:\sources>diskpart
DISKPART> sel disk 0
```

Next is to create our partitions:

```
DISKPART> create par efi size=100
DISKPART> create partition primary
```

If you get a message afterwards saying you need to convert your disk to GPT format, run the 
following and try to create your partitions again:

```
DISKPART> convert gpt
DISKPART> create par efi size=100
DISKPART> create partition primary
```

Run `list par` to see your partitions and begin formatting them:

```
DISKPART> sel par 1
DISKPART> format fs=fat32 quick
DISKPART> assign letter=g:
DISKPART> sel par 2
DISKPART> format fs=ntfs quick
DISKPART> assign letter=c:
```

You can see all of your volumes by running `list vol`. Once you're done, enter `exit` in the command
prompt to... exit.

Now we need to actually install Windows. To do so, go to the D: drive which contains the contents of
the ISO file:

```
X:\sources> d:
D:\> cd sources
```

The following command will show you what versions of Windows are available to be installed (Home,
Pro, etc.):

```
D:\sources> DISM /Get-ImageInfo /imagefile:install.wim
```

A list will appear; pick the one you want to install and remember its index. I install the Home 
edition which is usually index 1:

```
D:\sources> DISM /apply-image /imagefile:install.wim /index:1 /applydir:c:
```

All of the files from the ISO have been copied to our C: drive. The last thing we need to do is copy
the boot files to our EFI partition:

```
D:\sources> bcdboot c:\Windows /s G: /f ALL
```

You're done! Go ahead and reboot.

**Bonus**: When you reboot and it asks you to sign into a Microsoft account, you can bypass this by
hitting `Shift+F10` to open command prompt and entering `oobe\BypassNRO`. Your computer will reboot.
**Make sure you are not connected to the Internet**.

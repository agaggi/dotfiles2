{ config, pkgs, ... }:

{
    imports = [ ./packages.nix ];
    system.stateVersion = "22.11";

    # Nix settings
    nix = {
        settings = {
            auto-optimise-store = true;
            experimental-features = [ "nix-command" "flakes" ];
            use-xdg-base-directories = true;
        };

        gc = {
            automatic = true;
            dates = "weekly";
            options = "--delete-older-than 7d";
        };
    };

    # Bootloader
    boot = {
        kernelPackages = pkgs.linuxPackages;
        tmp.cleanOnBoot = true;
        loader = {
            grub.enable = true;
            grub.device = "/dev/sda";
        };
    };

    # User account
    i18n.defaultLocale = "en_US.UTF-8";
    time.timeZone = "America/New_York";

    networking.hostName = "frankenoof";

    users.users.alex = {
        extraGroups = [ "wheel" ];
        initialPassword = "Password";
        isNormalUser = true;
        openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICOLIT0QqNHkxm6caAIpdK+luIcR6rq6+PGCWnI/yv/C gaggiotti.alex@gmail.com" ];
        shell = pkgs.bash;
    };

    # Privilege escalation (use doas over sudo)
    security = {
        polkit.enable = true;
        sudo.enable = false;
        
        doas = {
            enable = true;
            wheelNeedsPassword = true;
            extraRules = [{ 
                groups = [ "wheel" ];
                keepEnv = true;
                persist = true; 
            }];
        };
    };

    # SSH
    services.openssh = {
        enable = true;
        settings = {
            PermitRootLogin = "no";
            PasswordAuthentication = false;
        };
    };

    # Other systemd services
    services.fstrim.enable = true;
    virtualisation.podman.enable = true;
}

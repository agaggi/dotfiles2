{ config, pkgs, ... }:

{
    environment.systemPackages = with pkgs; [
        btop
        curl
        docker-compose
        exfatprogs
        gcc
        git
        gnumake
        gzip
        man
        neovim
        rsync
        tmux
        unzip
        wget
        zip
        zstd
    ];
}

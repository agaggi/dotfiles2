{ config, pkgs, ... }:

{
    fonts.packages = with pkgs; [
        liberation_ttf
        (nerdfonts.override { fonts = [ "Noto" ]; })
        noto-fonts
        noto-fonts-cjk-sans
        noto-fonts-cjk-serif
        noto-fonts-emoji
        noto-fonts-extra
        unicode-character-database
    ];

    environment.systemPackages = with pkgs; [
        brave
        brightnessctl
        btop
        curl
        discord
        docker-compose
        dolphin-emu
        exfatprogs
        fd
        foot
        fuzzel
        gcc
        gimp
        git
        go
        gopls
        grim
        gruvbox-dark-gtk
        gzip
        imv
        libreoffice
        lime3ds
        lua-language-server
        mako
        man
        melonDS
        neovim
        obs-studio
        openssh
        playerctl
        pulsemixer
        pyright
        python3
        ripgrep
        rustup
        rsync
        slurp
        swaybg
        swaylock
        tmux
        unzip
        virt-manager
        vlc
        wget
        wl-clipboard
        wlr-randr
        yambar
        zig
        zip
        zstd
    ];
}

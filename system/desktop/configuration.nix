{ config, pkgs, ... }:

{
    imports = [ ./packages.nix ];
    system.stateVersion = "22.11";

    # Nix settings
    nixpkgs.config.allowUnfree = true;
    nix = {
        settings = {
            auto-optimise-store = true;
            experimental-features = [ "nix-command" "flakes" ];
            use-xdg-base-directories = true;
        };

        gc = {
            automatic = true;
            dates = "weekly";
            options = "--delete-older-than 7d";
        };
    };

    # Bootloader
    boot = {
        kernelPackages = pkgs.linuxPackages;
        tmp.cleanOnBoot = true;
        loader = {
            efi.canTouchEfiVariables = true;
            timeout = 3;
            systemd-boot = {
                enable = true;
                editor = false;
                consoleMode = "max";
                configurationLimit = 2;
            };
        };
    };

    # User account
    i18n.defaultLocale = "en_US.UTF-8";
    time.timeZone = "America/New_York";

    programs.fish.enable = true;

    users.users.alex = {
        extraGroups = [ "libvirtd" "wheel" ];
        initialPassword = "Password";
        isNormalUser = true;
        shell = pkgs.fish;
    };

    # Sound
    security.rtkit.enable = true;
    services.pipewire = {
        enable = true;

        alsa.enable = true;
        alsa.support32Bit = true;
        jack.enable = true;
        pulse.enable = true;
    };

    # Privilege escalation (use doas over sudo)
    security = {
        polkit.enable = true;
        sudo.enable = false;
        
        doas = {
            enable = true;
            wheelNeedsPassword = true;
            extraRules = [{ 
                groups = [ "wheel" ];
                keepEnv = true;
                persist = true; 
            }];
        };
    };

    # Window manager
    programs.river.enable = true;
    programs.river.xwayland.enable = true;
    
    xdg.portal = {
        enable = true;
        wlr.enable = true;
        extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };

    environment.variables.GTK_THEME = "gruvbox-dark";

    # Other systemd services
    networking.wireless.iwd.enable = true;
    services.fstrim.enable = true;
    services.resolved.enable = true;

    virtualisation.libvirtd.enable = true;
    virtualisation.podman.enable = true;

    # Misc.
    programs.steam.enable = true;
}

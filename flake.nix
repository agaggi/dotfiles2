{
    description = "Home system configurations";

    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    };

    outputs = { self, nixpkgs, ... }: {
        nixosConfigurations = {
            system = "x86_64-linux";
            
            desktop = nixpkgs.lib.nixosSystem {
                modules = [ 
                    ./system/desktop/configuration.nix
                    ./system/desktop/hardware-configuration.nix
                ];
            };

            laptop = nixpkgs.lib.nixosSystem {
                modules = [ 
                    ./system/laptop/configuration.nix
                    ./system/laptop/hardware-configuration.nix
                ];
            };

            server = nixpkgs.lib.nixosSystem {
                modules = [ 
                    ./system/server/configuration.nix
                    ./system/server/hardware-configuration.nix
                ];
            };
        };
    };
}
